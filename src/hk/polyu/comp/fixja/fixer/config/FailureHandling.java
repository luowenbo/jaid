package hk.polyu.comp.fixja.fixer.config;

/**
 * Created by Max PEI.
 */
public enum FailureHandling{
    CONTINUE, BREAK
}

